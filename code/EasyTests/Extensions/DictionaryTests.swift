//
//  DictionaryTests.swift
//  EasyTests
//
//  Created by David Batista on 22/01/21.
//

import XCTest

class DictionaryTests: XCTestCase {
    func testStringFromParameters() {
        let dict: [String: Any] = ["Parm1": 10.0, "Parm2": "Value2"]

        print(dict.stringFromHttpParameters())
        XCTAssertEqual(dict.stringFromHttpParameters(), "Parm1=10.0&Parm2=Value2")
    }
}
