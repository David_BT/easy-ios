//
//  InvestmentParameterTestes.swift
//  EasyTests
//
//  Created by David Batista on 18/01/21.
//

import XCTest

class InvestmentParameterTests: XCTestCase {
    func testJSONMapping() throws {
        let bundle = Bundle(for: type(of: self))

        guard let url = bundle.url(forResource: "InvestmentParameterMock", withExtension: "json"),
              let jsonData = try? Data(contentsOf: url) else {
            XCTFail("Missing file: InvestmentParameterMock.json")
            return
        }

        let decoder = JSONDecoder()
        let model = try decoder.decode(InvestmentParameter.self, from: jsonData)

        XCTAssertEqual(model.investedAmount, 32323.0)
        XCTAssertEqual(model.yearlyInterestRate, 9.5512)
        XCTAssertEqual(model.maturityTotalDays, 1981)
        XCTAssertEqual(model.maturityBusinessDays, 1409)
        XCTAssertEqual(model.maturityDate, "2023-03-03T00:00:00")
        XCTAssertEqual(model.rate, 123.0)
        XCTAssertEqual(model.isTaxFree, false)
    }
}
