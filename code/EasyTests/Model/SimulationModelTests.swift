//
//  SimulationModelTests.swift
//  EasyTests
//
//  Created by David Batista on 18/01/21.
//

import XCTest

class SimulationModelTests: XCTestCase {
    func testJSONMapping() throws {
        let bundle = Bundle(for: type(of: self))

        guard let url = bundle.url(forResource: "SimulationModelMock", withExtension: "json"),
              let jsonData = try? Data(contentsOf: url) else {
            XCTFail("Missing file: SimulationModelMock.json")
            return
        }

        let decoder = JSONDecoder()
        let model = try decoder.decode(SimulationModel.self, from: jsonData)

        XCTAssertEqual(model.grossAmount, 60528.20)
        XCTAssertEqual(model.taxesAmount, 4230.78)
        XCTAssertEqual(model.netAmount, 56297.42)
        XCTAssertEqual(model.grossAmountProfit, 28205.20)
        XCTAssertEqual(model.netAmountProfit, 23974.42)
        XCTAssertEqual(model.annualGrossRateProfit, 87.26)
        XCTAssertEqual(model.monthlyGrossRateProfit, 0.76)
        XCTAssertEqual(model.dailyGrossRateProfit, 0.000445330025305748)
        XCTAssertEqual(model.taxesRate, 15.0)
        XCTAssertEqual(model.rateProfit, 9.5512)
    }
}
