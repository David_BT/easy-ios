//
//  FormModelTests.swift
//  EasyTests
//
//  Created by Batista da Silva, David on 18/01/21.
//

import XCTest

class FormModelTests: XCTestCase {
    func testJSONMapping() throws {
        let bundle = Bundle(for: type(of: self))

        guard let url = bundle.url(forResource: "FormModelMock", withExtension: "json"),
              let jsonData = try? Data(contentsOf: url) else {
            XCTFail("Missing file: FormModelMock.json")
            return
        }

        let decoder = JSONDecoder()
        let model = try decoder.decode(FormModel.self, from: jsonData)

        XCTAssertEqual(model.investedAmount, 32323.0)
        XCTAssertEqual(model.index, "CDI")
        XCTAssertEqual(model.rate, 123)
        XCTAssertEqual(model.isTaxFree, false)
        XCTAssertEqual(model.maturityDate, "2023-03-03")
    }
}
