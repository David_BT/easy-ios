//
//  FormViewModelImplTests.swift
//  EasyTests
//
//  Created by David Batista on 22/01/21.
//

import XCTest

class FormViewModelTests: XCTestCase {
    func testSucessValidateDate() {
        let validDate = "20/05/2021"
        let viewModel: FormViewModel = FormViewModelImpl()

        XCTAssertTrue(viewModel.validateDate(validDate))
    }

    func testErrorValidateDate() {
        let validDate = "2021/01/20"
        let viewModel: FormViewModel = FormViewModelImpl()

        XCTAssertFalse(viewModel.validateDate(validDate))
    }

    func testWrongInvestedAmoundInValidateFields() {
        let investedAmount = "R$ 0,00"
        let maturityDate = "20/05/2021"
        let rate = "100%"
        let viewModel: FormViewModel = FormViewModelImpl()

        var response = viewModel.validateFields(investedAmount: investedAmount, maturityDate: maturityDate, rate: rate)

        XCTAssertFalse(response)

        response = viewModel.validateFields(investedAmount: "", maturityDate: maturityDate, rate: rate)

        XCTAssertFalse(response)
    }

    func testEmptyMaturityDateInValidateFields() {
        let investedAmount = "R$ 100,00"
        let rate = "100%"
        let viewModel: FormViewModel = FormViewModelImpl()

        let response = viewModel.validateFields(investedAmount: investedAmount, maturityDate: "", rate: rate)

        XCTAssertFalse(response)
    }

    func testEmptyRateInValidateFields() {
        let investedAmount = "R$ 100,00"
        let maturityDate = "20/05/2021"
        let viewModel: FormViewModel = FormViewModelImpl()

        let response = viewModel.validateFields(investedAmount: investedAmount, maturityDate: maturityDate, rate: "")

        XCTAssertFalse(response)
    }

    func testSuccessValidateFields() {
        let investedAmount = "R$ 120,00"
        let maturityDate = "20/05/2021"
        let rate = "100%"
        let viewModel: FormViewModel = FormViewModelImpl()

        let response = viewModel.validateFields(investedAmount: investedAmount,
                                                maturityDate: maturityDate,
                                                rate: rate)

        XCTAssertTrue(response)
    }

    func testEditingMaturityDateTextField() {
        let viewModel: FormViewModel = FormViewModelImpl()
        let textField = UITextField()

        textField.text = "01"
        _ = viewModel.maturityDateTextField(textField,
                                            shouldChangeCharactersIn: NSRange(location: 2, length: 0),
                                            replacementString: "0")
        XCTAssertEqual(textField.text, "01/")

        textField.text = "01/01"
        _ = viewModel.maturityDateTextField(textField,
                                            shouldChangeCharactersIn: NSRange(location: 5, length: 0),
                                            replacementString: "2")
        XCTAssertEqual(textField.text, "01/01/")

        textField.text = "01/01/2021"
        _ = viewModel.maturityDateTextField(textField,
                                            shouldChangeCharactersIn: NSRange(location: 5, length: 0),
                                            replacementString: "0")
        XCTAssertEqual(textField.text, "01/01/2021")
    }

    func testEditingRateTextField() {
        let viewModel: FormViewModel = FormViewModelImpl()
        let textField = UITextField()

        textField.text = "10"
        _ = viewModel.rateTextField(textField,
                                    shouldChangeCharactersIn: NSRange(location: 2, length: 0),
                                    replacementString: "0")
        XCTAssertEqual(textField.text, "100%")

        textField.text = "100%"
        _ = viewModel.rateTextField(textField,
                                    shouldChangeCharactersIn: NSRange(location: 4, length: 0),
                                    replacementString: "")
        XCTAssertEqual(textField.text, "10")

        textField.text = "100%"
        _ = viewModel.rateTextField(textField,
                                    shouldChangeCharactersIn: NSRange(location: 4, length: 0),
                                    replacementString: "0")
        XCTAssertEqual(textField.text, "100%")
    }
}
