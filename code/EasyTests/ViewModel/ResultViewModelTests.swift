//
//  ResultViewModelTests.swift
//  EasyTests
//
//  Created by David Batista on 22/01/21.
//

import XCTest

class ResultViewModelTests: XCTestCase {
    let form = FormModel(investedAmount: 100.0, rate: 120, maturityDate: "10/01/2020")

    func testFailRequestSimulation() {
        let service: SimulateService = SimulateServiceMock(withSuccess: false)
        let viewModel: ResultViewModel = ResultViewModelImpl(formParm: form, service: service)

        viewModel.requestSimulation { (success, message) in
            XCTAssertEqual(success, false)
            XCTAssertNotNil(message)
            guard let message = message else {
                XCTFail("Request success")
                return
            }
            XCTAssertEqual(message, "Erro desconhecido, tente novamente")
        }
    }

    func testSuccessRequestSimulation() {
        let service: SimulateService = SimulateServiceMock()
        let viewModel: ResultViewModel = ResultViewModelImpl(formParm: form, service: service)

        viewModel.requestSimulation { (success, message) in
            XCTAssertEqual(success, true)
            XCTAssertNil(message)
            XCTAssertNotNil(viewModel.simulationData)
        }
    }

    func testNumberOfSections() {
        let service: SimulateService = SimulateServiceMock()
        let viewModel: ResultViewModel = ResultViewModelImpl(formParm: form, service: service)

        XCTAssertEqual(viewModel.numberOfSections(), 4)
    }

    func testNumberOfRowsInSections() {
        let service: SimulateService = SimulateServiceMock()
        let viewModel: ResultViewModel = ResultViewModelImpl(formParm: form, service: service)

        XCTAssertEqual(viewModel.tableView(numberOfRowsInSection: 0), 2)
        XCTAssertEqual(viewModel.tableView(numberOfRowsInSection: 1), 5)
        XCTAssertEqual(viewModel.tableView(numberOfRowsInSection: 2), 6)
        XCTAssertEqual(viewModel.tableView(numberOfRowsInSection: 3), 1)
        XCTAssertEqual(viewModel.tableView(numberOfRowsInSection: 3), 1)
    }

    func testCellForRowAtFirstSection() {
        let service: SimulateService = SimulateServiceMock()
        let viewModel: ResultViewModel = ResultViewModelImpl(formParm: form, service: service)

        viewModel.requestSimulation { (success, _) in
            XCTAssertEqual(success, true)
        }

        let cellZero = viewModel.tableView(cellForRowAt: IndexPath(row: 0, section: 0))
        XCTAssertEqual(cellZero.title, "Resultado da simulação")
        XCTAssertNotNil(cellZero.value)
        XCTAssertEqual(cellZero.value!, "R$ 56.297,42")

        let cellOne = viewModel.tableView(cellForRowAt: IndexPath(row: 1, section: 0))
        XCTAssertEqual(cellOne.title, "Rendimento total de")
        XCTAssertNotNil(cellOne.value)
        XCTAssertEqual(cellOne.value!, "R$ 28.205,20")
    }

    func testCellForRowAtSecondSection() {
        let service: SimulateService = SimulateServiceMock()
        let viewModel: ResultViewModel = ResultViewModelImpl(formParm: form, service: service)

        viewModel.requestSimulation { (success, _) in
            XCTAssertEqual(success, true)
        }

        let cellZero = viewModel.tableView(cellForRowAt: IndexPath(row: 0, section: 1))
        XCTAssertEqual(cellZero.title, "Valor aplicado inicialmente")
        print(cellZero.value!)
        XCTAssertNotNil(cellZero.value)
        XCTAssertEqual(cellZero.value!, "R$ 32.323,00")

        let cellOne = viewModel.tableView(cellForRowAt: IndexPath(row: 1, section: 1))
        XCTAssertEqual(cellOne.title, "Valor bruto do investimento")
        XCTAssertNotNil(cellOne.value)
        XCTAssertEqual(cellOne.value!, "R$ 60.528,20")

        let cellTwo = viewModel.tableView(cellForRowAt: IndexPath(row: 2, section: 1))
        XCTAssertEqual(cellTwo.title, "Valor do rendimento")
        XCTAssertNotNil(cellTwo.value)
        XCTAssertEqual(cellTwo.value!, "R$ 28.205,20")

        let cellThree = viewModel.tableView(cellForRowAt: IndexPath(row: 3, section: 1))
        XCTAssertEqual(cellThree.title, "IR sobre o investimento")
        XCTAssertNotNil(cellThree.value)
        XCTAssertEqual(cellThree.value!, "R$ 4.230,78(15%)")

        let cellFour = viewModel.tableView(cellForRowAt: IndexPath(row: 4, section: 1))
        XCTAssertEqual(cellFour.title, "Valor líquido do investimento")
        XCTAssertNotNil(cellFour.value)
        XCTAssertEqual(cellFour.value!, "R$ 56.297,42")
    }

    func testCellForRowAtThirdSection() {
        let service: SimulateService = SimulateServiceMock()
        let viewModel: ResultViewModel = ResultViewModelImpl(formParm: form, service: service)

        viewModel.requestSimulation { (success, _) in
            XCTAssertEqual(success, true)
        }

        let cellZero = viewModel.tableView(cellForRowAt: IndexPath(row: 0, section: 2))
        XCTAssertEqual(cellZero.title, "Data de resgate")
        XCTAssertNotNil(cellZero.value)
        XCTAssertEqual(cellZero.value, "03/03/2023")

        let cellOne = viewModel.tableView(cellForRowAt: IndexPath(row: 1, section: 2))
        XCTAssertEqual(cellOne.title, "Dias corridos")
        XCTAssertNotNil(cellOne.value)
        XCTAssertEqual(cellOne.value, "1981")

        let cellTwo = viewModel.tableView(cellForRowAt: IndexPath(row: 2, section: 2))
        XCTAssertEqual(cellTwo.title, "Rendimento mensal")
        XCTAssertNotNil(cellTwo.value)
        XCTAssertEqual(cellTwo.value, "0.76%")

        let cellThree = viewModel.tableView(cellForRowAt: IndexPath(row: 3, section: 2))
        XCTAssertEqual(cellThree.title, "Percentual do CDI do investimento")
        XCTAssertNotNil(cellThree.value)
        XCTAssertEqual(cellThree.value, "123%")

        let cellFour = viewModel.tableView(cellForRowAt: IndexPath(row: 4, section: 2))
        XCTAssertEqual(cellFour.title, "Rentabilidade anual")
        XCTAssertNotNil(cellFour.value)
        XCTAssertEqual(cellFour.value, "87.26%")

        let cellFive = viewModel.tableView(cellForRowAt: IndexPath(row: 5, section: 2))
        XCTAssertEqual(cellFive.title, "Rentabilidade no periodo")
        XCTAssertNotNil(cellFive.value)
        XCTAssertEqual(cellFive.value, "9.55%")
    }

    func testCellForRowAtFourthSection() {
        let service: SimulateService = SimulateServiceMock()
        let viewModel: ResultViewModel = ResultViewModelImpl(formParm: form, service: service)

        viewModel.requestSimulation { (success, _) in
            XCTAssertEqual(success, true)
        }

        let cellZero = viewModel.tableView(cellForRowAt: IndexPath(row: 0, section: 3))
        XCTAssertEqual(cellZero.title, "Simular novamente")
        XCTAssertNotNil(cellZero.value)
        XCTAssertEqual(cellZero.value, "")
    }
}
