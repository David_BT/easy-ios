//
//  SimulateServiceTests.swift
//  EasyTests
//
//  Created by David Batista on 25/01/21.
//

import XCTest

class SimulateServiceTests: XCTestCase {
    func testGetSimulationError() {
        let service = SimulateServiceImpl()
        let expectation = XCTestExpectation(description: "Simulate form")

        service.simulate(withParm: ["Parm1": 1000, "Parm2": "05/01/2021"]) { (result) in

            switch result {
            case .success:
                XCTFail("Wrong parameters returns success")
            case .failure:
                expectation.fulfill()
            }
        }

        wait(for: [expectation], timeout: 1.0)
    }

    func testGetSimulationSuccess() {
        let service = SimulateServiceImpl()
        let expectation = XCTestExpectation(description: "Simulate form")
        let form = FormModel(investedAmount: 100.4, rate: 103, maturityDate: "05/01/2021")

        service.simulate(withParm: form.withParameter()) { (result) in

            switch result {
            case .success:
                expectation.fulfill()
            case .failure(let error):
                XCTFail("Error in simulate form :: \(error.localizedDescription)")
            }
        }

        wait(for: [expectation], timeout: 1.0)
    }
}
