//
//  SimulateServiceMock.swift
//  EasyTests
//
//  Created by David Batista on 23/01/21.
//

import Foundation

class SimulateServiceMock: SimulateService {
    let returnSuccess: Bool

    init(withSuccess: Bool = true) {
        returnSuccess = withSuccess
    }

    func simulate(withParm parm: [String: Any],
                  responseHandler: @escaping (Result<SimulationModel, APIError>) -> Void) {
        guard returnSuccess else {
            responseHandler(.failure(.badRequest("Erro no retorno da API")))
            return
        }
        let bundle = Bundle(for: type(of: self))

        guard let url = bundle.url(forResource: "SimulationModelMock", withExtension: "json"),
              let jsonData = try? Data(contentsOf: url) else {
            responseHandler(.failure(.badRequest(nil)))
            return
        }

        do {
            let model = try JSONDecoder().decode(SimulationModel.self, from: jsonData)
            responseHandler(.success(model))
        } catch {
            responseHandler(.failure(.parseError(error.localizedDescription)))
        }
    }
}
