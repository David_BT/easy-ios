//
//  AppDelegate.swift
//  Easy
//
//  Created by Batista da Silva, David on 18/01/21.
//

import UIKit
import IQKeyboardManagerSwift

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        startFormScreen()
//        startResultScreen()

        IQKeyboardManager.shared.enable = true

        return true
    }

    private func startFormScreen() {
        let window = UIWindow(frame: UIScreen.main.bounds)

        let formViewController = FormViewController(viewModel: FormViewModelImpl())
        let navigation = UINavigationController(rootViewController: formViewController)
        navigation.isNavigationBarHidden = true

        window.rootViewController = navigation
        window.makeKeyAndVisible()
        self.window = window
    }

    private func startResultScreen() {
        let window = UIWindow(frame: UIScreen.main.bounds)

        let resultViewModel = ResultViewModelImpl(formParm: FormModel(investedAmount: 1200.0,
                                                                      rate: 110,
                                                                      maturityDate: "20/01/2030"),
                                                  service: SimulateServiceImpl())
        let resultViewController = ResultTableViewController(viewModel: resultViewModel)

        window.rootViewController = resultViewController
        window.makeKeyAndVisible()
        self.window = window
    }

    // MARK: UISceneSession Lifecycle
    @available(iOS 13.0, *)
    func application(_ application: UIApplication,
                     configurationForConnecting connectingSceneSession: UISceneSession,
                     options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
}
