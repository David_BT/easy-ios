//
//  SceneDelegate.swift
//  Easy
//
//  Created by Batista da Silva, David on 18/01/21.
//

import UIKit

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?

    func scene(_ scene: UIScene,
               willConnectTo session: UISceneSession,
               options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }

        let window = UIWindow(windowScene: windowScene)
        let formViewController = FormViewController(viewModel: FormViewModelImpl())
        let navigation = UINavigationController(rootViewController: formViewController)
        navigation.isNavigationBarHidden = true

        window.rootViewController = navigation
        window.makeKeyAndVisible()

        self.window = window
    }
}
