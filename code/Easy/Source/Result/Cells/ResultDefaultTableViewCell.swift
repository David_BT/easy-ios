//
//  ResultDefaultTableViewCell.swift
//  Easy
//
//  Created by David Batista on 20/01/21.
//

import UIKit

class ResultDefaultTableViewCell: UITableViewCell {
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var valueLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setupCell(withTitle title: String, andValue value: String?) {
        self.titleLabel.text = title
        if let value = value {
            self.valueLabel.text = value
            self.valueLabel.stopShimmering()
        } else {
            self.valueLabel.text = ""
            self.valueLabel.startShimmering()
        }
    }
}
