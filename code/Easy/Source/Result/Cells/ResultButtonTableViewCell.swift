//
//  ResultButtonTableViewCell.swift
//  Easy
//
//  Created by David Batista on 21/01/21.
//

import UIKit

class ResultButtonTableViewCell: UITableViewCell {
    @IBOutlet weak var simulateButton: UIButton!

    var simulateButtonAction: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()

        simulateButton.layer.cornerRadius = simulateButton.frame.height / 2
    }

    func setupCell(withAction actionButton: @escaping () -> Void) {
        simulateButtonAction = actionButton
    }

    @IBAction func simulateButtonTapped() {
        self.simulateButtonAction?()
    }
}
