//
//  ResultProfitTableViewCell.swift
//  Easy
//
//  Created by David Batista on 21/01/21.
//

import UIKit

class ResultProfitTableViewCell: UITableViewCell {
    @IBOutlet private weak var profitLabel: UILabel!

    func setupCell(withTitle title: String, andValue value: String?) {
        if let value = value {
            let mutableString = NSMutableAttributedString(string: "\(title) \(value)",
                                                          attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12),
                                                                       NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            mutableString.addAttribute(NSAttributedString.Key.foregroundColor,
                                       value: UIColor(named: "AppGreen") ?? UIColor.green,
                                       range: NSRange(location: title.count + 1, length: value.count))
            self.profitLabel.attributedText = mutableString
            self.profitLabel.stopShimmering()
        } else {
            self.profitLabel.text = ""
            self.profitLabel.startShimmering()
        }
    }
}
