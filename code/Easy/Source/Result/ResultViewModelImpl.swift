//
//  ResultViewModel.swift
//  Easy
//
//  Created by David Batista on 20/01/21.
//

import Foundation

class ResultViewModelImpl: ResultViewModel {
    internal let formParm: FormModel
    internal let service: SimulateService
    internal var simulationData: SimulationModel?

    private var rowsInSections = [2, 5, 6, 1]

    required init(formParm: FormModel, service: SimulateService) {
        self.formParm = formParm
        self.service = service
    }

    func requestSimulation(completionHandler: @escaping (Bool, String?) -> Void) {
        self.service.simulate(withParm: formParm.withParameter()) { (requestResult) in
            switch requestResult {
            case .success(let data):
                self.simulationData = data
                completionHandler(true, nil)
            case .failure(let error):
                var messageError = ""
                switch error {
                case .serverError:
                    messageError = "Estamos com problemas em nossos serviços. Tente novamente em instantes"
                default:
                    messageError = "Erro desconhecido, tente novamente"
                }

                completionHandler(false, messageError)
            }
        }
    }

    func numberOfSections() -> Int {
        return rowsInSections.count
    }

    func tableView(numberOfRowsInSection section: Int) -> Int {
        guard let rows = rowsInSections[safe: section] else { fatalError("Invalid section numbers") }

        return rows
    }

    func tableView(cellForRowAt indexPath: IndexPath) -> (title: String, value: String?) {
        switch indexPath.section {
        case 0:
            return rowElementInFirstSection(indexPath.row)
        case 1:
            return rowElementInSecondSection(indexPath.row)
        case 2:
            return rowElementInThirdSection(indexPath.row)
        case 3:
            return rowElementInFourthSection(indexPath.row)
        default:
            fatalError("Section \(indexPath.section) hasn't been implemented")
        }
    }
}

// MARK: - Private Methods
extension ResultViewModelImpl {
    private func rowElementInFirstSection(_ row: Int) -> (title: String, value: String?) {
        var title = ""
        var value: String?

        switch row {
        case 0:
            title = "Resultado da simulação"
            if let netAmount = simulationData?.netAmount {
                value = currencyString(from: netAmount)
            }
        case 1:
            title = "Rendimento total de"
            if let grossAmountProfit = simulationData?.grossAmountProfit {
                value = currencyString(from: grossAmountProfit)
            }
        default:
            fatalError("Row \(row) hasn't been implemented in section 1")
        }

        return (title: title, value: value)
    }

    private func rowElementInSecondSection(_ row: Int) -> (title: String, value: String?) {
        var title = ""
        var value: String?

        switch row {
        case 0:
            title = "Valor aplicado inicialmente"
            if let investedAmount = simulationData?.investmentParameter.investedAmount {
                value = currencyString(from: investedAmount)
            }
        case 1:
            title = "Valor bruto do investimento"
            if let grossAmount = simulationData?.grossAmount {
                value = currencyString(from: grossAmount)
            }

        case 2:
            title = "Valor do rendimento"
            if let grossAmountProfit = simulationData?.grossAmountProfit {
                value = currencyString(from: grossAmountProfit)
            }

        case 3:
            title = "IR sobre o investimento"
            if let taxesAmount = simulationData?.taxesAmount, let taxesRate = simulationData?.taxesRate {
                value = "\(currencyString(from: taxesAmount))(\(taxesRate.percent)%)"
            }
        case 4:
            title = "Valor líquido do investimento"
            if let netAmount = simulationData?.netAmount {
                value = currencyString(from: netAmount)
            }
        default:
            fatalError("Row \(row) hasn't been implemented in section 2")
        }

        return (title: title, value: value)
    }

    private func rowElementInThirdSection(_ row: Int) -> (title: String, value: String?) {
        var title = ""
        var value: String?

        switch row {
        case 0:
            title = "Data de resgate"
            if let maturityDate = simulationData?.investmentParameter.maturityDate {
                if let date = DateFormatter(dateFormat: "yyyy-MM-dd'T'HH:mm:ss").date(from: maturityDate) {
                    value = DateFormatter(dateFormat: "dd/MM/yyyy").string(from: date)
                } else {
                    value = "Data inválida"
                }
            }

        case 1:
            title = "Dias corridos"
            if let maturityTotalDays = simulationData?.investmentParameter.maturityTotalDays {
                value = String(maturityTotalDays)
            }

        case 2:
            title = "Rendimento mensal"
            if let monthlyGrossRateProfit = simulationData?.monthlyGrossRateProfit {
                value = "\(monthlyGrossRateProfit.percent)%"
            }

        case 3:
            title = "Percentual do CDI do investimento"
            if let rate = simulationData?.investmentParameter.rate {
                value = "\(rate.percent)%"
            }

        case 4:
            title = "Rentabilidade anual"
            if let annualGrossRateProfit = simulationData?.annualGrossRateProfit {
                value = "\(annualGrossRateProfit.percent)%"
            }

        case 5:
            title = "Rentabilidade no periodo"
            if let rateProfit = simulationData?.rateProfit {
                value = "\(rateProfit.percent)%"
            }

        default:
            fatalError("Row \(row) hasn't been implemented in section 2")
        }

        return (title: title, value: value)
    }

    private func rowElementInFourthSection(_ row: Int) -> (title: String, value: String) {
        switch row {
        case 0:
            return (title: "Simular novamente", value: "")
        default:
            fatalError("Row \(row) hasn't been implemented in section 4")
        }
    }

    private func currencyString(from value: Float?) -> String {
        guard let value = value else { return "R$ 0,00" }

        let currencyFormatter = NumberFormatter(numberStyle: .currency)
        currencyFormatter.locale = Locale(identifier: "pt_BR")

        return currencyFormatter.string(from: NSNumber(value: value)) ?? "R$ 0,00"
    }
}
