//
//  ResultViewController.swift
//  Easy
//
//  Created by Batista da Silva, David on 18/01/21.
//

import UIKit

class ResultViewController: UIViewController {
    // MARK: - ViewModel
    var viewModel: ResultViewModel

    // MARK: - View Controller Life Cycle

    init(viewModel: ResultViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
