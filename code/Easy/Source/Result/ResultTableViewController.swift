//
//  ResultTableViewController.swift
//  Easy
//
//  Created by David Batista on 20/01/21.
//

import UIKit

class ResultTableViewController: UITableViewController {
    // MARK: - ViewModel
    var viewModel: ResultViewModel

    // MARK: - View Controller Life Cycle
    init(viewModel: ResultViewModel) {
        self.viewModel = viewModel

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) hasn't been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupViewUI()
        bindingViewModel()
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.tableView(numberOfRowsInSection: section)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellFields = viewModel.tableView(cellForRowAt: indexPath)

        switch indexPath.section {
        case 0:
            return dequeueHeaderCells(withTitle: cellFields.title, andValue: cellFields.value, into: indexPath)

        case 3:
            return dequeueButtonCell(into: indexPath)

        default:
            return dequeueDefaultCell(withTitle: cellFields.title, andValue: cellFields.value, into: indexPath)
        }
    }
}

// MARK: - Helper Methods
extension ResultTableViewController {
    private func setupViewUI() {
        tableView.register(UINib(nibName: "ResultDefaultTableViewCell", bundle: nil),
                           forCellReuseIdentifier: "ResultDefaultTableViewCell")
        tableView.register(UINib(nibName: "ResultSimulationTableViewCell", bundle: nil),
                           forCellReuseIdentifier: "ResultSimulationTableViewCell")
        tableView.register(UINib(nibName: "ResultProfitTableViewCell", bundle: nil),
                           forCellReuseIdentifier: "ResultProfitTableViewCell")
        tableView.register(UINib(nibName: "ResultButtonTableViewCell", bundle: nil),
                           forCellReuseIdentifier: "ResultButtonTableViewCell")

        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 80
    }

    private func bindingViewModel() {
        viewModel.requestSimulation { (success, messageError) in
            guard success else {
                self.showDialog(withMessage: messageError ?? "Erro desconhecido, tente novamente!", title: nil)
                return
            }

            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }

    private func showDialog(withMessage message: String, title: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Continuar", style: .default, handler: { (_) in
            self.navigationController?.popViewController(animated: true)
        }))

        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }

    private func dequeueHeaderCells(withTitle title: String,
                                    andValue value: String?,
                                    into indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ResultSimulationTableViewCell",
                                                           for: indexPath) as? ResultSimulationTableViewCell
            else { fatalError("ResultSimulationTableViewCell hasn't been implemented") }

            cell.setupCell(withTitle: title, andValue: value)

            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ResultProfitTableViewCell", for: indexPath)
                    as? ResultProfitTableViewCell
            else { fatalError("ResultProfitTableViewCell hasn't been implemented") }

            cell.setupCell(withTitle: title, andValue: value)

            return cell
        }
    }

    private func dequeueDefaultCell(withTitle title: String,
                                    andValue value: String?,
                                    into indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ResultDefaultTableViewCell", for: indexPath)
                as? ResultDefaultTableViewCell else { fatalError("ResultDefaultTableViewCell hasn't been implemented") }

        cell.setupCell(withTitle: title, andValue: value)

        return cell
    }

    private func dequeueButtonCell(into indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ResultButtonTableViewCell", for: indexPath)
                as? ResultButtonTableViewCell else { fatalError("ResultButtonTableViewCell hasn't been implemented") }

        cell.setupCell(withAction: {
            self.navigationController?.popViewController(animated: true)
        })

        return cell
    }
}
