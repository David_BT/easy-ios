//
//  FormViewModel.swift
//  Easy
//
//  Created by David Batista on 19/01/21.
//

import Foundation
import UIKit

class FormViewModelImpl: FormViewModel {
    var form: FormModel?

    func validateDate(_ date: String) -> Bool {
        guard !date.isEmpty else { return true }

        let dateFormatter = DateFormatter(dateFormat: "dd/MM/yyyy")
        guard let date = dateFormatter.date(from: date) else { return false }

        return date > Date()
    }

    func validateFields(investedAmount: String, maturityDate: String, rate: String) -> Bool {
        guard investedAmount != "R$ 0,00" && !investedAmount.isEmpty else { return false }
        guard !maturityDate.isEmpty else { return false }
        guard rate.digits.count >= 3 else { return false }

        let formatter = NumberFormatter(numberStyle: .currency)
        formatter.locale = Locale(identifier: "pt_BR")
        guard let investedDecimal = formatter.number(from: investedAmount) else { return false }

        form = FormModel(investedAmount: investedDecimal.doubleValue,
                         rate: rate.int,
                         maturityDate: maturityDate)

        return true
    }

    func maturityDateTextField(_ textField: UITextField,
                               shouldChangeCharactersIn range: NSRange,
                               replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }

        if (text.count == 2) || (text.count == 5) {
            if !string.isEmpty {
                textField.text = text + "/"
            }
        }

        return !(text.count > 9 && (string.count) > range.length)
    }

    func rateTextField(_ textField: UITextField,
                       shouldChangeCharactersIn range: NSRange,
                       replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }

        if string.isEmpty {
            if text.count == 4 {
                textField.text = text.dropLast(2).string
                return false
            }
        } else {
            if text.count == 2 {
                textField.text = text + string + "%"
                return false
            }
        }

        return !(text.count > 3 && (string.count) > range.length)
    }
}
