//
//  FormViewController.swift
//  Easy
//
//  Created by Batista da Silva, David on 18/01/21.
//

import UIKit

class FormViewController: UIViewController {
    // MARK: - IBOutlets
    @IBOutlet weak var investedAmountTextField: CurrencyField!
    @IBOutlet weak var maturityDateTextField: UITextField!
    @IBOutlet weak var rateTextField: UITextField!
    @IBOutlet weak var simulateButton: UIButton!
    @IBOutlet weak var maturityDateDivisorView: UIView!
    @IBOutlet weak var maturityDateErrorLabel: UILabel!

    // MARK: - ViewModel
    var viewModel: FormViewModel

    // MARK: - View Controller Life Cycle
    init(viewModel: FormViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupViewUI()
    }

    private func setupViewUI() {
        maturityDateErrorLabel.isHidden = true
        investedAmountTextField.textAlignment = .center
        simulateButton.layer.cornerRadius = simulateButton.frame.height / 2
        simulateButton.isEnabled = false
        simulateButton.backgroundColor = .gray
    }

    @IBAction func simulateButtonTapped() {
        guard let formParm = viewModel.form else { return }

        let resultViewModel = ResultViewModelImpl(formParm: formParm, service: SimulateServiceImpl())
        let resultViewController = ResultTableViewController(viewModel: resultViewModel)
        self.navigationController?.pushViewController(resultViewController, animated: true)
    }
}

extension FormViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == maturityDateTextField {
            if viewModel.validateDate(textField.string) {
                maturityDateDivisorView.backgroundColor = UIColor.AppColor.appDivisor
                maturityDateErrorLabel.isHidden = true
            } else {
                maturityDateDivisorView.backgroundColor = .red
                maturityDateErrorLabel.isHidden = false
                return
            }
        }

        if viewModel.validateFields(investedAmount: investedAmountTextField.string,
                                    maturityDate: maturityDateTextField.string,
                                    rate: rateTextField.string) {
            simulateButton.isEnabled = true
            simulateButton.backgroundColor = UIColor.AppColor.appGreen
        } else {
            simulateButton.isEnabled = false
            simulateButton.backgroundColor = .gray
        }
    }

    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        switch textField {
        case rateTextField:
            return viewModel.rateTextField(textField, shouldChangeCharactersIn: range,
                                           replacementString: string)
        case maturityDateTextField:
            return viewModel.maturityDateTextField(textField, shouldChangeCharactersIn: range,
                                                   replacementString: string)
        default:
            return true
        }
    }
}
