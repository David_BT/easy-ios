//
//  ColorExtension.swift
//  Easy
//
//  Created by David Batista on 20/01/21.
//

import UIKit
import Foundation

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")

        self.init(red: CGFloat(red) / 255.0,
                  green: CGFloat(green) / 255.0,
                  blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }

    struct AppColor {
        static let appGreen = UIColor(named: "AppGreen")
        static let appDivisor = UIColor(named: "AppDivisor")
    }
}
