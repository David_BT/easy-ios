//
//  CurrencyFieldExtension.swift
//  Easy
//
//  Created by David Batista on 18/01/21.
//

import Foundation

extension CurrencyField {
    var doubleValue: Double { (decimal as NSDecimalNumber).doubleValue }
}
