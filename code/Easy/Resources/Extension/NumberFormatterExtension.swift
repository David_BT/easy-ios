//
//  NumberFormatterExtension.swift
//  Easy
//
//  Created by David Batista on 18/01/21.
//

import Foundation

extension NumberFormatter {
    convenience init(numberStyle: Style) {
        self.init()
        self.numberStyle = numberStyle
    }
}
