//
//  DictionaryExtension.swift
//  Easy
//
//  Created by David Batista on 22/01/21.
//

extension Dictionary {
    func stringFromHttpParameters() -> String {
        let parameterArray = self.map { "\($0)=\($1)" }.sorted()

        print(parameterArray)

        return parameterArray.joined(separator: "&")
    }
}
