//
//  ArrayExtension.swift
//  Easy
//
//  Created by David Batista on 20/01/21.
//

import Foundation

extension Array {
    subscript (safe index: Index) -> Element? {
        return 0 <= index && index < count ? self[index] : nil
    }
}
