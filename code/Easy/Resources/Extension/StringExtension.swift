//
//  StringExtension.swift
//  Easy
//
//  Created by David Batista on 18/01/21.
//

import Foundation

extension String {
    var decimal: Decimal { Decimal(string: digits) ?? 0 }
    var int: Int { Int(digits) ?? 0 }
}
