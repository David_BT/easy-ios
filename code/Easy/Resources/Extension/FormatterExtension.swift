//
//  FormatterExtension.swift
//  Easy
//
//  Created by David Batista on 18/01/21.
//

import Foundation

extension Formatter {
    static let currency: NumberFormatter = .init(numberStyle: .currency)
    static let decimal: NumberFormatter = .init(numberStyle: .decimal)
}
