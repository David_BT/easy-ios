//
//  DecimalExtension.swift
//  Easy
//
//  Created by David Batista on 18/01/21.
//

import UIKit

extension Decimal {
    var currency: String { Formatter.currency.string(for: self) ?? "" }
}
