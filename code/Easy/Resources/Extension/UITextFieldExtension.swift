//
//  UITextFieldExtension.swift
//  Easy
//
//  Created by David Batista on 18/01/21.
//

import UIKit

extension UITextField {
     var string: String { text ?? "" }
}
