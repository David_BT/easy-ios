//
//  FloatExtension.swift
//  Easy
//
//  Created by David Batista on 21/01/21.
//

import Foundation

extension Float {
    var percent: String {
        let formatter = Formatter.decimal
        formatter.maximumFractionDigits = 2

        return formatter.string(for: self) ?? "0"
    }
}
