//
//  LosslessStringConvertible.swift
//  Easy
//
//  Created by David Batista on 18/01/21.
//

import Foundation

extension LosslessStringConvertible {
    var string: String { .init(self) }
}
