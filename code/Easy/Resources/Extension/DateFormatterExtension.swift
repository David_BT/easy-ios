//
//  DateFormatterExtension.swift
//  Easy
//
//  Created by David Batista on 19/01/21.
//

import Foundation

extension DateFormatter {
    convenience init(dateFormat: String) {
        self.init()
        self.dateFormat = dateFormat
    }
}
