//
//  UIViewExtension.swift
//  Easy
//
//  Created by David Batista on 21/01/21.
//

import UIKit

extension UIView {
    private static var lastBackgroundColor: UIColor?

    func startShimmering() {
        UIView.lastBackgroundColor = self.backgroundColor
        backgroundColor = UIColor(named: "AppDivisor")

        let lightColor = UIColor(displayP3Red: 1.0, green: 1.0, blue: 1.0, alpha: 0.1).cgColor
        let blackColor = UIColor.black.cgColor

        // Create a CAGradientLayer  ->3
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [blackColor, lightColor, blackColor]
        gradientLayer.frame = CGRect(x: -self.bounds.size.width,
                                     y: -self.bounds.size.height,
                                     width: 3 * self.bounds.size.width,
                                     height: 3 * self.bounds.size.height)

            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)

        gradientLayer.locations =  [0.35, 0.50, 0.65]
        self.layer.mask = gradientLayer

        CATransaction.begin()
        let animation = CABasicAnimation(keyPath: "locations")
        animation.fromValue = [0.0, 0.1, 0.2]
        animation.toValue = [0.8, 0.9, 1.0]
        animation.duration = CFTimeInterval(1.4)
        animation.repeatCount = MAXFLOAT
        CATransaction.setCompletionBlock { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.layer.mask = nil
        }
        gradientLayer.add(animation, forKey: "shimmerAnimation")
        CATransaction.commit()
    }

    func stopShimmering() {
        self.backgroundColor = UIView.lastBackgroundColor
        self.layer.mask = nil
    }
}
