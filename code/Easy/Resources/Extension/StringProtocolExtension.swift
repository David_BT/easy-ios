//
//  StringProtocolExtension.swift
//  Easy
//
//  Created by David Batista on 18/01/21.
//

import Foundation

extension StringProtocol where Self: RangeReplaceableCollection {
    var digits: Self { filter(\.isWholeNumber) }
}
