//
//  CurrencyField.swift
//  Easy
//
//  Created by David Batista on 18/01/21.
//

import UIKit

class CurrencyField: UITextField {
    var decimal: Decimal { string.decimal / pow(10, Formatter.currency.maximumFractionDigits) }
    var maximum: Decimal = 999_999_999.99

    private var lastValue: String?

    var locale: Locale = .current {
        didSet {
            Formatter.currency.locale = locale
            sendActions(for: .editingChanged)
        }
    }

    override func willMove(toSuperview newSuperview: UIView?) {
        self.locale = Locale(identifier: "pt_BR")

        Formatter.currency.locale = locale
        addTarget(self, action: #selector(editingChanged), for: .editingChanged)
        keyboardType = .numberPad
        textAlignment = .right
        sendActions(for: .editingChanged)
    }

    override func deleteBackward() {
        text = string.digits.dropLast().string

        sendActions(for: .editingChanged)
    }

    @objc
    func editingChanged() {
        guard decimal <= maximum else {
            text = lastValue
            return
        }

        text = decimal.currency
        lastValue = text
    }
}
