//
//  HTTPRequest.swift
//  Easy
//
//  Created by David Batista on 22/01/21.
//

import Foundation

class HTTPRequest {
    class func request(url urlString: String,
                       parameters: [String: Any],
                       completionHandler: @escaping (_ data: Data?,
                                                     _ response: URLResponse?,
                                                     _ error: Error?) -> Void) {
        let parameterString = parameters.stringFromHttpParameters()
        guard let url = URL(string: "\(urlString)?\(parameterString)") else {
            return completionHandler(nil, nil, NSError())
        }

        var request = URLRequest(url: url)
        request.timeoutInterval = 30

        let task = URLSession.shared.dataTask(with: request, completionHandler: completionHandler)
        task.resume()
    }

    class func parse<T: Codable>(data: Data) -> Result<T, APIError> {
        do {
            let decoder = JSONDecoder()
            let model = try decoder.decode(T.self, from: data)
            return .success(model)
        } catch {
            return .failure(APIError.parseError(error.localizedDescription))
        }
    }
}
