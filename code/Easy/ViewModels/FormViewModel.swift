//
//  FormViewModel.swift
//  Easy
//
//  Created by David Batista on 20/01/21.
//

import Foundation
import UIKit

protocol FormViewModel {
    var form: FormModel? { get }

    func validateDate(_ date: String) -> Bool
    func validateFields(investedAmount: String, maturityDate: String, rate: String) -> Bool
    func maturityDateTextField(_ textField: UITextField,
                               shouldChangeCharactersIn range: NSRange,
                               replacementString string: String) -> Bool
    func rateTextField(_ textField: UITextField,
                       shouldChangeCharactersIn range: NSRange,
                       replacementString string: String) -> Bool
}
