//
//  ResultViewModel.swift
//  Easy
//
//  Created by David Batista on 20/01/21.
//

import Foundation

protocol ResultViewModel {
    var formParm: FormModel { get }
    var service: SimulateService { get }
    var simulationData: SimulationModel? { get }

    init(formParm: FormModel, service: SimulateService)

    func numberOfSections() -> Int
    func tableView(numberOfRowsInSection section: Int) -> Int
    func tableView(cellForRowAt indexPath: IndexPath) -> (title: String, value: String?)
    func requestSimulation(completionHandler: @escaping (Bool, String?) -> Void)
}
