//
//  GetSimulation.swift
//  Easy
//
//  Created by David Batista on 22/01/21.
//

import Foundation

protocol SimulateService {
    func simulate(withParm parm: [String: Any],
                  responseHandler: @escaping (_ response: Result<SimulationModel, APIError>) -> Void)
}

struct SimulateServiceImpl: SimulateService {
    private let urlString = "https://api-simulator-calc.easynvest.com.br/calculator/simulate"

    func simulate(withParm parm: [String: Any],
                  responseHandler: @escaping (_ response: Result<SimulationModel, APIError>) -> Void) {
        HTTPRequest.request(url: urlString, parameters: parm) { (data, request, error) in
            guard let urlResponse = request as? HTTPURLResponse else {
                responseHandler(.failure(APIError.serverError(error?.localizedDescription)))
                return
            }

            responseHandler(verifyStatusCode(data: data, urlResponse: urlResponse, error: error))
        }
    }

    private func verifyStatusCode(data: Data?,
                                  urlResponse: HTTPURLResponse,
                                  error: Error?) -> Result<SimulationModel, APIError> {
        switch urlResponse.statusCode {
        case 200...299:
            if let data = data {
                return HTTPRequest.parse(data: data)
            } else {
                return .failure(APIError.noData)
            }
        case 400...499:
            return .failure(APIError.badRequest(error?.localizedDescription))
        case 500...599:
            return .failure(APIError.serverError(error?.localizedDescription))
        default:
            return .failure(APIError.unknown)
        }
    }
}
