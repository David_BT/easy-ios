//
//  FormModel.swift
//  Easy
//
//  Created by Batista da Silva, David on 18/01/21.
//

struct FormModel: Codable {
    var investedAmount: Double
    var index: String = "CDI"
    var rate: Int
    var isTaxFree: Bool = false
    var maturityDate: String

    init(investedAmount: Double, rate: Int, maturityDate: String) {
        self.investedAmount = investedAmount
        self.rate = rate
        self.maturityDate = maturityDate
    }

    func withParameter() -> [String: Any] {
        return ["investedAmount": investedAmount,
                "index": index,
                "rate": rate,
                "isTaxFree": isTaxFree,
                "maturityDate": maturityDate]
    }
}
