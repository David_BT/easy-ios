//
//  InvestmentParameter.swift
//  Easy
//
//  Created by Batista da Silva, David on 18/01/21.
//

struct InvestmentParameter: Codable {
    let investedAmount: Float
    let yearlyInterestRate: Float
    let maturityTotalDays: Int
    let maturityBusinessDays: Int
    let maturityDate: String
    let rate: Float
    let isTaxFree: Bool
}
