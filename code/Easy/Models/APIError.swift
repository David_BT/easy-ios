//
//  APIError.swift
//  Easy
//
//  Created by David Batista on 22/01/21.
//

enum APIError: Error {
    case noData
    case invalidaResponse
    case badRequest(String?)
    case serverError(String?)
    case parseError(String?)
    case unknown
}
