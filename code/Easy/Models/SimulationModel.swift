//
//  SimulationModel.swift
//  Easy
//
//  Created by Batista da Silva, David on 18/01/21.
//

struct SimulationModel: Codable {
    let investmentParameter: InvestmentParameter
    let grossAmount: Float
    let taxesAmount: Float
    let netAmount: Float
    let grossAmountProfit: Float
    let netAmountProfit: Float
    let annualGrossRateProfit: Float
    let monthlyGrossRateProfit: Float
    let dailyGrossRateProfit: Float
    let taxesRate: Float
    let rateProfit: Float
    let annualNetRateProfit: Float
}
